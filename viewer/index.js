/* eslint max-len: ["error", { "code": 120 }] */

const dataDir = 'data/json/current';
const attachmentsDir = 'data/attachments/full';

/** The entry point */
async function main() { // eslint-disable-line no-unused-vars
  const data = indexAndJoinData(await fetchData());
  console.log(data);

  window.addEventListener('hashchange', () => route(data));
  route(data);

  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox(); // eslint-disable-line no-invalid-this
  });
};


/** Fetch all JSON data and return it in a single object */
async function fetchData() {
  return Promise.all(
      [
        fetch(`${dataDir}/assessment_groups.json`),
        fetch(`${dataDir}/conversation.json`),
        fetch(`${dataDir}/messages.json`),
        fetch(`${dataDir}/notes.json`),
        fetch(`${dataDir}/weight_trackings.json`),
        fetch(`${dataDir}/workouts.json`),
      ].map((p) => p.then((f) => f.json())),
  ).then((parts) => Object.assign({}, ...parts));
};


/**
 * Index and join the data so it's ready for use
 * @param {Object} data - raw data
 * @return {Object} data - indexed and joined data
 * */
function indexAndJoinData(data) {
  // index

  data['workoutItemsById'] = Object.fromEntries(data['workout_items'].map((i) => [i.id, i]));

  data['commentsById'] = Object.fromEntries(data['comments'].map((c) => [c.id, c]));
  data['imagesById'] = Object.fromEntries(data['images'].map((i) => [i.id, i]));
  data['usersById'] = Object.fromEntries(data['users'].map((u) => [u.id, u]));
  data['usersByTypeAndId'] = Object.assign({},
      Object.fromEntries(data['trainers'].map((t) => [['trainer', t.id], data['usersById'][t.user_id]])),
      Object.fromEntries(data['clients'].map((c) => [['client', c.id], data['usersById'][c.user_id]])),
  );

  data['assessmentsById'] = Object.fromEntries(data['assessments'].map((a) => [a.id, a]));
  data['assessmentItemsById'] = Object.fromEntries(data['assessment_items'].map((i) => [i.id, i]));
  data['assessmentGroupsInOrder'] = [...data['assessment_groups']].sort((a, b) => a.order - b.order);

  // join

  data['workouts'].forEach((w) => {
    Object.assign(w, {
      workout_items: w.workout_item_ids.map((id) =>
        data['workoutItemsById'][id],
      ).sort((a, b) => a.position - b.position),
    });
  });

  data['users'].forEach((u) => {
    Object.assign(u, {image: data['imagesById'][u.image_id]});
  });
  data['comments'].forEach((c) => {
    Object.assign(c, {user: data['usersByTypeAndId'][[c.commenter.type, c.commenter.id]]});
  });
  data['messages'].forEach((m) => {
    Object.assign(m, {user: data['usersByTypeAndId'][[m.sender.type.toLowerCase(), m.sender.id]]});
  });

  data['assessment_groups'].forEach((g) => {
    Object.assign(g, {
      assessments: g.assessment_ids.map((id) => data['assessmentsById'][id]).sort((a, b) => a.order - b.order),
    });
  });
  data['assessments'].forEach((a) => {
    Object.assign(a, {
      assessment_items: a.assessment_item_ids.map((id) =>
        data['assessmentItemsById'][id],
      ).sort((a, b) => a.date.localeCompare(b.date)),
    });
  });

  return data;
}


/**
 * Routing
 * @param {Object} data
 * */
function route(data) {
  const defaultPage = 'workouts';
  const hashPath = document.location.hash.replace(/^\#/, '') || defaultPage;
  const page = hashPath.split('/')[0];

  const renderer = {
    workouts: () => renderWorkoutsPage(data),
    summary: () => renderSummaryPage(data),
    messages: () => renderMessagesPage(data),
    metrics: () => renderMetricsPage(data),
  }[page];

  $('li.nav-item.active').removeClass('active');
  $(`li.nav-item a[href$=${page}]`).closest('li.nav-item').addClass('active');

  $('#filters').html('');

  renderer();
};


/* -------------------------------------------------------------------------
 *  Page renderers
 * ------------------------------------------------------------------------- */


/**
 * Render the Workouts page
 * @param {Object} data
 */
function renderWorkoutsPage(data) {
  const filtersTpl = `
    <ul class="navbar-nav ml-auto">
      <li class="mx-1 my-1">${dateRangePickerTpl()}</li>
    </ul>
  `;
  $('#filters').html(filtersTpl);

  $('#content').html(workoutsPageTpl());

  const config = dateRangePickerConfig(data);
  const startEndConfig = generateStartEndConfig(config.nonPluginConfig.workoutsDefault);
  $('#daterangepicker').daterangepicker({...config, ...startEndConfig});
  $('#daterangepicker').on('apply.daterangepicker', () => renderWorkouts(data));

  /**
   * Render the workouts
   * @param {Object} data - all data
   */
  function renderWorkouts(data) {
    const [start, end] = updateLocationHashFromDateRangePicker('workouts', config.nonPluginConfig.workoutsDefault);

    const workoutsTpl = [...data.workouts].filter((w) => {
      return w.due >= start && w.due <= end;
    }).map((w) => workoutTpl(w, data)).join('\n');

    $('#workouts').html(workoutsTpl);

    // preload the videos for the first workout that has any
    $('.workout video').first().closest('.workout').find('video').attr('preload', 'auto');
  }

  renderWorkouts(data);
};


/**
 * Render the Summary page
 * @param {Object} data - all data
 */
function renderSummaryPage(data) {
  const filtersTpl = `
    <ul class="navbar-nav ml-auto">
      <li class="mx-1 my-1">${exerciseSelectPickerTpl(exerciseNamesData(data['workout_items']))}</li>
      <li class="mx-1 my-1">${dateRangePickerTpl()}</li>
    </ul>
  `;
  $('#filters').html(filtersTpl);

  const tpl = summaryPageTpl();
  $('#content').html(tpl);

  const config = dateRangePickerConfig(data);
  const startEndConfig = generateStartEndConfig(config.nonPluginConfig.summaryDefault);
  $('#daterangepicker').daterangepicker({...config, ...startEndConfig});
  $('#daterangepicker').on('apply.daterangepicker', () => renderSummary(data));

  $('#selectpicker').on('changed.bs.select', () => renderSummary(data));
  $('#selectpicker').selectpicker(selectPickerConfig());

  /**
   * Render summary table
   * @param {Object} data
   */
  function renderSummary(data) {
    const selected = new Set($('#selectpicker').val());
    const [start, end] = updateLocationHashFromDateRangePicker('summary', config.nonPluginConfig.summaryDefault);

    const normalize = (name) => name.replace(/[\W ]+/g, '');
    const trimLong = (s) => trimLinesAt(s, 40);

    const rows = [];
    [...data.workouts].filter((w) => {
      return w.due >= start && w.due <= end;
    }).forEach((workout) => {
      workout.workout_items.filter((i) => selected.has(normalize(i.name))).forEach((item) => {
        rows.push([
          workoutLinkTpl(workout.due),
          trimLong(item.name),
          checkIcon(item.state == 'completed'),
          bodyTextTpl(trimLong(item.info)),
          bodyTextTpl(trimLong(item.result)),
        ]);
      });
    });

    const tpl = summaryTableTpl(rows);
    $('#summary').html(tpl);
  }

  renderSummary(data);
};


/**
 * Render the Messages page
 * @param {Object} data - all data
 */
function renderMessagesPage(data) {
  const tpl = messagesTpl([...data.messages].reverse());
  $('#content').html(tpl);
};


/**
 * Render the Metrics page
 * @param {Object} data - all data
 */
function renderMetricsPage(data) {
  const tpl = metricsTpl(data['assessmentGroupsInOrder']);
  $('#content').html(tpl);
};


/* -------------------------------------------------------------------------
 *  Helpers
 * ------------------------------------------------------------------------- */


/**
 * Generate the local attachment URL
 * @param {string} url - original URL
 * @return {string} local URL
 */
function localUrl(url) {
  const fileBase = (new Hashes.SHA1).hex(url);
  const fileExt = url.split('.').pop();
  return attachmentsDir + '/' + fileBase + '.' + fileExt;
};


/**
 * Prepare config for the date range picker
 * @param {Object} data - all data
 * @return {Object} config
 */
function dateRangePickerConfig(data) {
  const stateByDate = Object.fromEntries(data.workouts.map((w) => [w.due, w.state]));

  const datesSet = new Set(data.workouts.map((w) => w.due));

  const datesSorted = [...datesSet].sort();
  const first = datesSorted[0];
  const last = datesSorted[datesSorted.length-1];
  const firstMoment = moment(first, moment.ISO_8601, true);
  const lastMoment = moment(last, moment.ISO_8601, true);
  const fmt = 'YYYY-MM-DD';
  const seventhLast = lastMoment.clone().subtract(6, 'days').format(fmt);

  const yearRanges = Object.fromEntries(
      [...Array(lastMoment.year() - firstMoment.year() + 1).keys()].
          map((offset) => firstMoment.year() + offset).
          reverse().
          map((year) => [`${year} `, [`${year}-01-01`, `${year}-12-31`]]),
      // The trailing space in the year key is to preserve ordering
  );

  return {
    'nonPluginConfig': {
      'workoutsDefault': [seventhLast, last],
      'summaryDefault': [first, last],
    },
    'ranges': {
      'Last day': [last, last],
      'Last 7 days': [seventhLast, last],
      ...yearRanges,
      'First day': [first, first],
      'All time': [first, last],
    },
    'locale': {
      'format': fmt,
      'separator': ' -- ',
      'applyLabel': 'Apply',
      'cancelLabel': 'Cancel',
      'fromLabel': 'From',
      'toLabel': 'To',
      'customRangeLabel': 'Custom',
      'weekLabel': 'W',
      'daysOfWeek': ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      'monthNames': [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December',
      ],
      'firstDay': 1,
    },
    'alwaysShowCalendars': true,
    'linkedCalendars': true,
    'autoApply': true,
    'minDate': first,
    'maxDate': last,
    'opens': 'left',
    'isInvalidDate': (date) => !datesSet.has(date.format(fmt)),
    'isCustomDate': (date) => {
      const state = stateByDate[date.format(fmt)];
      if (state) {
        return ['cal-day-workout-' + state];
      } else {
        return [];
      }
    },
  };
};


/**
 * Prepare config for the select picker
 * @return {Object} config
 */
function selectPickerConfig() {
  return {
    actionsBox: true,
    liveSearch: true,
    selectAllText: 'All',
    deselectAllText: 'None',
    selectedTextFormat: 'count > 0',
    countSelectedText: (m, n) => m == n ? `Exercises (All)` : `Exercises (${m}/${n})`,
    noneSelectedText: 'Exercises (None)',
    style: 'btn-secondary',
  };
}


/**
 * Generate startDate and endDate config base don the location hash and defaults
 * @param {Array} defaultRange - in case the location hash has no dates
 * @return {Object} config - to merge with other date range picker config
 */
function generateStartEndConfig(defaultRange) {
  const afterPage = document.location.hash.replace(/^\#(\w+)?\/?/, '');
  const [start, end] = afterPage.split('/')[0].split(/--/);

  if (!start && !end) {
    return {startDate: defaultRange[0], endDate: defaultRange[1]};
  } else if (start && !end) {
    return {startDate: start, endDate: start};
  } else if (start && end) {
    return {startDate: start, endDate: end};
  } else {
    alert('Invalid date range!');
  }
};


/**
 * Update the location hash from the date range picker and return the new range
 * @param {String} page - name of the current page
 * @param {Array} defaultRange - the default date range of the current page
 * @return {Array} range - new date range from the date range picker
 */
function updateLocationHashFromDateRangePicker(page, defaultRange) {
  const toStr = (range) => [...new Set(range)].join('--');
  const range = $('#daterangepicker').val().split(' -- ');
  const rangeStr = toStr(range);

  let newHash;
  if (rangeStr == toStr(defaultRange)) {
    newHash = `#${page}`;
  } else {
    newHash = `#${page}/${rangeStr}`;
  }

  if (newHash != window.location.hash) {
    const newURL = window.location.origin + window.location.pathname + newHash;
    history.pushState({}, '', newURL);
  }
  return range;
};


/**
 * Generate exercise name data
 * @param {Array} workoutItems
 * @return {Array} exercise names - [[normalized, display], ...]
 */
function exerciseNamesData(workoutItems) {
  const uniqueNames = [...new Set(workoutItems.map((i) => i.name.trim()))];

  const singleWordNames = new Set(
      uniqueNames
          .map((s) => s.toLowerCase())
          .filter((n) => n.split(' ').length == 1),
  );

  const results = uniqueNames.map((name) => {
    const normalizedName = name.replace(/[\W ]+/g, '');

    const words = name.trim().split(' ');
    const lastWord = words[words.length-1];
    const normalizedLastWord = lastWord.toLowerCase().replace(/([^s])s$/, '$1');

    let displayName;
    if (words.length > 1 && singleWordNames.has(normalizedLastWord)) {
      displayName = lastWord + ', ' + words.slice(0, words.length-1).join(' ');
    } else {
      displayName = name;
    };

    return [normalizedName, displayName];
  });

  return results.sort((a, b) => a[1].localeCompare(b[1]));
}


/* -------------------------------------------------------------------------
 *  View templates
 * ------------------------------------------------------------------------- */


/**
 * Template for the date range picker input
 * @return {string} string
 */
function dateRangePickerTpl() {
  return `
    <input id="daterangepicker" class="btn btn-secondary"></input>
  `;
}


/**
 * Template for the summary page
 * @param {Array} exNamesData - exercise names data [[normalized, display], ...]
 * @return {string} string
 */
function exerciseSelectPickerTpl(exNamesData) {
  const options = exNamesData.map((pair) => {
    return `<option value="${pair[0]}" selected>${pair[1]}</option>`;
  }).join('\n');

  return `
    <select id="selectpicker" multiple>
      ${options}
    </select>
  `;
}


/**
 * Template for the summary page
 * @return {string} string
 */
function summaryPageTpl() {
  return `
    <div id="summary" class="container pt-3"></div>
  `;
}


/**
 * Template for the summary table
 * @param {Array} rowsData - data for the rows of the table
 * @return {string} string
 */
function summaryTableTpl(rowsData) {
  return `
    <table class="table">
      <thead>
        <tr>
          <th scope="col" width="10%">Due</th>
          <th scope="col" width="25%">Exercise</th>
          <th scope="col" width="5%"></th>
          <th scope="col" width="30%">Plan</th>
          <th scope="col" width="30%">Actual</th>
        </tr>
      </thead>
      ${tableBodyTpl(rowsData)}
    </table>
  `;
}


/**
 * Template for the workouts page
 * @return {string} string
 */
function workoutsPageTpl() {
  return `
    <div id="workouts" class="container" style="max-width: 540px;"></div>
  `;
}


/**
 * Template for a workout
 * @param {Object} workout
 * @param {Object} data - all data
 * @return {string} string
 */
function workoutTpl(workout, data) {
  return `
    <div class="workout container">

      <div class="row mt-3">
        <h2 class="">
          <span class="">${workout.title || 'Workout'}</span>
        </h2>
      </div>
      <div class="row">
        <span class="pl-1 pr-2">${checkIcon(workout.state == 'completed')}</span>
        <span class="">${dateTpl(workout.due)}</span>
      </div>

      <div class="exercises mt-4">
        ${workout.workout_items.map((item) => exerciseTpl(item, data)).join('\n')}
      </div>

      <div class="comments pt-2">
        ${[...workout.comment_ids].reverse().map((id) => commentTpl(data.commentsById[id])).join('\n')}
      </div>

    </div>
  `;
};


/**
 * Template for an exercise
 * @param {Number} item - workout item
 * @param {Object} data - all data
 * @return {string} string
 */
function exerciseTpl(item, data) {
  return `
    <div class="exercise card my-3">
      <div class="card-header">
        <h6 class="mb-0">
          <span class="">${item.name}</span>
          <span class="float-right">${checkIcon(item.state == 'completed')}</span>
        </h6>
        <div class=""></div>
      </div>
      <div class="card-body">
        <div class="card-text">
          <div class="pb-1">
            <table class="">
              <tr class="text-muted">
                <td class="font-weight-bold pr-4 align-text-top">Plan</td>
                <td class="text-break">${bodyTextTpl(item.info)}</td>
              </tr>
              <tr>
                <td class="font-weight-bold pr-4 align-text-top">Actual</td>
                <td class="text-break">${bodyTextTpl(item.result)}</td>
              </tr>
            </table>
          </div>
        </div>
        ${attachmentsTpl(item.attachments, data)}
        </ul>
      </div>
    </div>
  `;
};


/**
 * Template for attachments
 * @param {Object} attachments
 * @param {Object} data - all data
 * @return {string} string
 */
function attachmentsTpl(attachments, data) {
  return `
    <ul class="attachments list-group list-group-flush px-0">
      ${attachments.map((a) => attachmentTpl(a, data)).join('\n')}
    </ul>
  `;
};


/**
 * Template for an attachment
 * @param {Object} attachment
 * @param {Object} data - all data
 * @return {string} string
 */
function attachmentTpl(attachment, data) {
  const typeType = attachment.type.split('/')[0];

  let content;
  switch (typeType) {
    case 'video':
      content = `
        <video class="img-fluid" preload="none" controls onmouseover="$(this).attr('preload', 'auto')">
          <source src="${localUrl(attachment.attachmentUrl)}" type="${attachment.type}">
        </video>
        <div class="attachment-name text-muted"><small>${attachment.name}</small></div>
      `;
      break;
    case 'image':
    case 'gif':
      content = `
        <a href="${localUrl(attachment.attachmentUrl)}"
           data-toggle="lightbox"
           data-footer="<small>${attachment.name}</small>" />
          <img class="img-fluid" src="${localUrl(attachment.attachmentUrl)}" />
          <div class="attachment-name text-muted"><small>${attachment.name}</small></div>
        </a>
      `;
      break;
    default:
      content = `
        <a class="btn btn-block btn-secondary" href="${localUrl(attachment.attachmentUrl)}" download>
          <div class="media">
            <div class="my-auto mx-2 py-1">${fileDownloadIcon()}</div>
            <div class="media-body my-auto"><small>${attachment.name}</small></div>
          </div>
        </a>
      `;
  }

  return `
    <li class="attachment list-group-item border-0 px-0">
      ${content}
    </li>
  `;
};


/**
 * Template for a comment
 * @param {Number} comment - comment or message
 * @return {string} string
 */
function commentTpl(comment) {
  return `
    <div class="py-3">
      <div class="comment media">
        <img src="${localUrl(comment.user.image.image_url)}"
             alt="${comment.user.first_name}"
             class="mr-3 rounded-circle"
             style="width: 64px; height: 64px;" />
        <div class="media-body">
          <h5 class="mt-0">${comment.user.first_name} ${comment.user.last_name}</h5>
          <h6 class="mt-0">${datetimeTpl(comment.created_at)}</h6>
          ${comment.body ? '<p class="text-break pt-1">' + bodyTextTpl(comment.body) + '</p>' : ''}
          ${attachmentsTpl(comment.attachments)}
        </div>
      </div>
    </div>
  `;
};


/**
 * Template for the messages
 * @param {Object} messagesInOrder - the relevant data
 * @return {String} string
 */
function messagesTpl(messagesInOrder) {
  return `
    <div class="container">
      <div class="comments pt-2 px-2">
        ${messagesInOrder.map(commentTpl).join('\n')}
      </div>
    </div>
  `;
};


/**
 * Template for the metrics
 * @param {Object} assessmentGroupsInOrder - the relevant data
 * @return {String} string
 */
function metricsTpl(assessmentGroupsInOrder) {
  const rows = [];
  assessmentGroupsInOrder.forEach((group) => {
    group.assessments.forEach((assessment) => {
      assessment.assessment_items.forEach((item) => {
        const itemDate = item.date.substr(0, 10);
        rows.push([
          group.name,
          assessment.name,
          item.value,
          assessment.units,
          bodyTextTpl(item.note),
          workoutLinkTpl(itemDate),
        ]);
      });
    });
  });

  return `
    <div class="container py-3">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Group</th>
            <th scope="col">Assessment</th>
            <th scope="col">Value</th>
            <th scope="col">Units</th>
            <th scope="col">Note</th>
            <th scope="col">Date</th>
          </tr>
        </thead>
        ${tableBodyTpl(rows)}
      </table>
    </div>
  `;
};


/**
 * Render body text to HTML
 * @param {String} text
 * @return {string} string
 */
function bodyTextTpl(text) {
  return text.replace(/\n/g, '<br/>');
};


/**
 * Render body text to HTML
 * @param {String} text
 * @param {String} limit - maximum length
 * @return {string} string
 */
function trimLinesAt(text, limit) {
  return text.split('\n').map((l) => {
    if (l.length <= limit) {
      return l;
    } else {
      return l.substr(0, limit-3) + '...';
    }
  }).join('\n');
};


/**
 * Reformat datetime
 * @param {String} input - YYYY-MM-DDThh:mm:ss.SSSSSSZ
 * @return {string} formatted - 13 November 2020, 07:24:00
 */
function datetimeTpl(input) {
  return new Date(input).toLocaleDateString('en-gb',
      {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        timeZone: 'UTC',
      },
  );
};


/**
 * Reformat date
 * @param {String} input - YYYY-MM-DDThh:mm:ss.SSSSSSZ
 * @return {string} formatted - 14 November 2020
 */
function dateTpl(input) {
  return new Date(input).toLocaleDateString('en-gb',
      {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        timeZone: 'UTC',
      },
  );
};


/**
 * Workout link template
 * @param {String} date - date of workout as YYYY-MM-DD
 * @return {string} html
 */
function workoutLinkTpl(date) {
  return `<span class="text-nowrap"><a href="#workouts/${date}">${date}</a></span>`;
};


/**
 * Table body template
 * @param {Array} rows
 * @return {string} formatted
 */
function tableBodyTpl(rows) {
  const field = (value) => `<td>${value}</td>`;
  const row = (fields) => `<tr>${fields.map(field).join('')}</tr>`;
  return `
    <tbody>
      ${rows.map(row).join('\n')}
    </tbody>
  `;
};


/**
 * Checked or unchecked icon
 * @param {Boolean} filled
 * @return {string} html
 */
function checkIcon(filled) {
  if (filled) {
    return `
      <svg width="1em" height="1em" viewBox="0 0 16 16"
           class="bi bi-check-circle-fill" fill="currentColor"
           xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd"
              d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0
                 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97
                 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0
                 0-.01-1.05z"/>
      </svg>
    `;
  } else {
    return `
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle"
           fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd"
              d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0
                 0 0 16z"/>
      </svg>
    `;
  };
};

/**
 * File earmark arrow down SVG icon
 * @return {string} html
 */
function fileDownloadIcon() {
  return `
    <svg width="2em" height="2em" viewBox="0 0 16 16"
         class="bi bi-file-earmark-arrow-down" fill="currentColor"
         xmlns="http://www.w3.org/2000/svg">
      <path d="M4 0h5.5v1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0
               1-1V4.5h1V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2z"/>
      <path d="M9.5 3V0L14 4.5h-3A1.5 1.5 0 0 1 9.5 3z"/>
      <path fill-rule="evenodd" d="M8 6a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5
            0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 1 1 .708-.708L7.5
            10.293V6.5A.5.5 0 0 1 8 6z"/>
    </svg>
  `;
}
