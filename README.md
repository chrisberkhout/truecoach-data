# TrueCoach data fetcher

Fetch and view client data from [TrueCoach](https://truecoach.co/).

## Introduction

It's good to have a machine-readable copy of your data offline as a backup and
for custom analysis. TrueCoach doesn't have a data export feature.

This fetcher will log in and download client data in JSON format, and download
all the attached videos and other files. Each time it is run it will download a
fresh snapshot of all the JSON data, and download any attached files that
aren't already downloaded. All the data are stored in the `./data` directory.

For me a year worth of TrueCoach client data is about 1.5 MB of uncompressed
JSON, and about 30 GB of attachements.

I've tried to fetch all the client data in TrueCoach. The `assessments` link is
left out because it is included in `assessment_groups`. The following links are
left out because for me they were each just an empty array or object:
`daily_nutrition_logs`, `health_trackings`, `nutrition_plan`, `photo_sessions`,
`skeletons`, and (the unauthorized) `stripe_subscriptions`.

There's a viewer for exploring the data in a web browser:

![Viewer demo](viewer-demo.gif)

## Getting started

Install Python dependencies using [Pipenv](https://pipenv.pypa.io/en/latest/).

Set your TrueCoach credentials in the environment variables `TRUECOACH_USER`
and `TRUECOACH_PASS` (consider using [direnv](https://direnv.net/)).

Run (GNU) `make` to see a list of tasks defined by the `Makefile`.

Run `make run` to do a full update of TrueCoach client data.

Run `make view` serve the viewer and open it in a browser.
