#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Summarize attachments information from JSON data and downloaded files
'''

from argparse import ArgumentParser
import json
import os
from hashlib import sha1

def main(args):
    info = attachment_info(args.data)
    results = check(info, args.files)
    if args.all:
        summary = results
    else:
        summary = [ r for r in list(results) if not r['matches'] ]
    summary_json = json.dumps(summary, indent=4)
    print(summary_json)

def normalize_type(type):
    if type == 'gif':
        return 'image/gif'
    else:
        return type

def attachment_info(json_dir):
    attachments = []
    with open(f'{json_dir}/workouts.json') as f:
      workouts = json.load(f)
      attachments.extend(extract_attachment_info(workouts['comments']))
      attachments.extend(extract_attachment_info(workouts['workout_items']))
      for image in workouts['images']:
          attachments.append({
              'url': image['image_url'],
              'size': int(image['file_size']),
              'type': normalize_type(image['mime_type'])
          })
    with open(f'{json_dir}/assessment_groups.json') as f:
      assessment_groups = json.load(f)
      attachments.extend(extract_attachment_info(assessment_groups['assessment_items']))
    with open(f'{json_dir}/messages.json') as f:
      messages = json.load(f)
      attachments.extend(extract_attachment_info(messages['messages']))
    with open(f'{json_dir}/notes.json') as f:
      notes = json.load(f)
      attachments.extend(extract_attachment_info(notes['notes']))
    return attachments

def extract_attachment_info(items):
    info = []
    for item in items:
        for a in item['attachments']:
            info.append({
                'url': a['attachmentUrl'],
                'size': int(a['size']),
                'type': normalize_type(a['type'])
            })
    return info

def check(attachments, attachments_dir):
    results = []
    for a in attachments:
        file_base = sha1(a['url'].encode('utf-8')).hexdigest()
        file_ext = a['url'].split('.')[-1]
        filename = attachments_dir + '/' + file_base + '.' + file_ext
        exists = os.path.isfile(filename)
        actual_size = os.stat(filename).st_size if exists else None
        matches = a['size'] == actual_size
        a['file'] = filename
        results.append({
            'url': a['url'],
            'size': a['size'],
            'file': filename,
            'type': a['type'],
            'exists': exists,
            'actual_size': actual_size,
            'matches': matches
        })
    return results

def parse_args(parser):
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-m', '--mismatches', help='print only mismatch info (default)', action='store_true')
    group.add_argument('-a', '--all', help='print all info', action='store_true')
    parser.add_argument('-d', '--data', help="JSON data directory (default: 'data/json/current')", default='data/json/current')
    parser.add_argument('-f', '--files', help="attachment files directory (default: 'data/attachments/full')", default='data/attachments/full')
    parsed = parser.parse_args()
    return parsed

if __name__ == '__main__':
    args = parse_args(ArgumentParser())
    main(args)
