help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

run: scrape link attachments ## Do a full update of TrueCoach client data

scrape: ## Only run the scraper
	time pipenv run scrapy crawl client

link: ## Only update the link at data/json/current to point to the latest json
	cd data/json && find -type d | sort | tail -1 | xargs -I{} ln -sfT {} current

attachments: ## Only generate a summary of attachments from current data and downloads
	pipenv run scripts/attachments_info.py -a > data/json/current/attachments.json

check-attachments: ## Show mismatches between attachments data and downloads
	pipenv run scripts/attachments_info.py --mismatches

view: ## Serve and open the viewer
	(sleep 1 && xdg-open 'http://localhost:5000/' &); pipenv run python -m http.server 5000 -d viewer/
