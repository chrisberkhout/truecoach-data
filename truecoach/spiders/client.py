import scrapy
import os
import urllib.parse
import logging

class ClientSpider(scrapy.Spider):
    name = 'client'

    base_url = 'https://app.truecoach.co/proxy'
    per_page = 1000

    def start_requests(self):
        if ('TRUECOACH_USER' not in os.environ or
            'TRUECOACH_PASS' not in os.environ):
            logging.critical("Environment variables TRUECOACH_USER and TRUECOACH_PASS must be set")
            return []

        url = f"{self.base_url}/api/oauth/token"
        return [scrapy.FormRequest(url, formdata={
                    'grant_type': 'password',
                    'username': os.environ['TRUECOACH_USER'],
                    'password': os.environ['TRUECOACH_PASS'],
                })]

    def parse(self, response):
        auth = response.json()

        headers = {
            'Role': 'Client',
            'Authorization': f"{str.capitalize(auth['token_type'])} {auth['access_token']}",
        }

        pagination = '?' + urllib.parse.urlencode({ 'page': 1, 'per_page': self.per_page })

        workouts_url = f"{self.base_url}/api/clients/{auth['user_id']}/workouts{pagination}"
        yield scrapy.Request(workouts_url, headers=headers, callback=self.parse_workouts)

        conversation_url = f"{self.base_url}/api/clients/{auth['user_id']}/conversation"
        yield scrapy.Request(conversation_url, headers=headers, callback=self.parse_conversation)

        notes_url = f"{self.base_url}/api/clients/{auth['user_id']}/notes{pagination}"
        yield scrapy.Request(notes_url, headers=headers, callback=self.parse_notes)

        assessment_groups_url = f"{self.base_url}/api/clients/{auth['user_id']}/assessment_groups{pagination}"
        yield scrapy.Request(assessment_groups_url, headers=headers, callback=self.parse_assessment_groups)

        weight_trackings_url = f"{self.base_url}/api/clients/{auth['user_id']}/weight_trackings{pagination}"
        yield scrapy.Request(weight_trackings_url, headers=headers, callback=self.parse_weight_trackings)


    def parse_workouts(self, response):
        workouts = response.json()

        if workouts['meta']['per_page'] != self.per_page:
            raise scrapy.exceptions.CloseSpider(f"Request of {per_page} items per page was overridden")
        if workouts['meta']['total_count'] == self.per_page:
            raise scrapy.exceptions.CloseSpider(f"Total items may exceed the requested {per_page}")

        file_urls = []

        for comment in workouts['comments']:
            for attachment in comment['attachments']:
                file_urls.append(attachment['attachmentUrl'])

        for image in workouts['images']:
            file_urls.append(image['image_url'])

        for workout_item in workouts['workout_items']:
            for attachment in workout_item['attachments']:
                file_urls.append(attachment['attachmentUrl'])

        yield {
            'json_name': 'workouts',
            'json_data': workouts,
            'file_urls': file_urls,
        }


    def parse_conversation(self, response):
        conversation = response.json()

        headers = response.request.headers
        meta = {
            'messages_base_url': f"{self.base_url}/api/messages",
            'params': { 'conversation_id': conversation['conversation']['id'], 'page': 1},
            'messages': [],
        }
        messages_url = meta['messages_base_url'] + '?' + urllib.parse.urlencode(meta['params'])
        yield scrapy.Request(messages_url, headers=response.request.headers, meta=meta, callback=self.parse_messages)

        yield {
            'json_name': 'conversation',
            'json_data': conversation,
        }


    def parse_messages(self, response):
        messages = response.json()
        if messages['messages']:
            meta = response.meta
            meta['messages'].extend(messages['messages'])
            meta['params']['page'] += 1
            messages_url = meta['messages_base_url'] + '?' + urllib.parse.urlencode(meta['params'])
            yield scrapy.Request(messages_url, headers=response.request.headers, meta=meta, callback=self.parse_messages)
        else:
            messages_acc = response.meta['messages']
            file_urls = []
            for message in messages_acc:
                for attachment in message['attachments']:
                    file_urls.append(attachment['attachmentUrl'])
            yield {
                'json_name': 'messages',
                'json_data': { 'messages': messages_acc },
                'file_urls': file_urls,
            }


    def parse_notes(self, response):
        notes = response.json()
        file_urls = []
        for note in notes['notes']:
            for attachment in note['attachments']:
                file_urls.append(attachment['attachmentUrl'])
        yield {
            'json_name': 'notes',
            'json_data': notes,
            'file_urls': file_urls,
        }


    def parse_assessment_groups(self, response):
        assessment_groups = response.json()
        file_urls = []
        for item in assessment_groups['assessment_items']:
            for attachment in item['attachments']:
                file_urls.append(attachment['attachmentUrl'])
        yield {
            'json_name': 'assessment_groups',
            'json_data': assessment_groups,
            'file_urls': file_urls,
        }

    def parse_weight_trackings(self, response):
        yield {
            'json_name': 'weight_trackings',
            'json_data': response.json(),
        }
