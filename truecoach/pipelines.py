# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import json
import logging
import pathlib
from hashlib import sha1
import os


class JSONPipeline:
    def process_item(self, item, spider):
        if 'json_data' in item:
            path = spider.settings['JSON_STORE'] + '/'
            name = item['json_name'] + '.json'
            filename = path + name
            pathlib.Path(filename).parent.mkdir(parents=True, exist_ok=True)
            with open(filename, 'w') as f:
                json.dump(item['json_data'], fp=f, indent=2)
            logging.info(f"Saved {item['json_name']} data to {filename}")

        return item

class SkipExistingFilesPipeline:
    def process_item(self, item, spider):
        new_file_urls = []
        for url in item.get('file_urls', []):
            file_base = sha1(url.encode('utf-8')).hexdigest()
            file_ext = url.split('.')[-1]
            filename = spider.settings['FILES_STORE'] + '/full/' + file_base + '.' + file_ext
            if not os.path.isfile(filename):
                new_file_urls.append(url)
        item['file_urls'] = new_file_urls

        return item
